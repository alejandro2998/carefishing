import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ToastController } from 'ionic-angular';
import { InformePage } from '../informe/informe';
import { SeleccionPage } from '../seleccion/seleccion';

@IonicPage()
@Component({
  selector: 'page-size',
  templateUrl: 'size.html',
})
export class SizePage {
  picudo:string;
  data: string;
  datosForm:String[];
  datosFoto: any[];
  fecha: Date;
  medida: string;
  peso: string;
  gender: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public toast: ToastController) {
    this.data = navParams.get('data');
    this.datosForm = navParams.get('datosForm');
    this.datosFoto = navParams.get('datosFoto');
    this.fecha = navParams.get('timestamp');
    this.medida = navParams.get('medida');
    this.peso = navParams.get('peso');

    console.log("Picudo Capturado");
    console.log(this.data);
    console.log("Datos obtenidos anteriormente");
    console.log(this.datosForm);
    console.log(this.datosFoto);
    console.log("Timestamp capturado: ");
    console.log(this.fecha);
  }
  

  ionViewDidLoad() {
     console.log('Se ha cargado correctamente página de selección de picudos!');
  }
  probarResult(){

    console.log(this.picudo);
    if(this.medida != undefined && this.peso != undefined){
      this.navCtrl.setRoot(InformePage, {data:this.data, datosForm:this.datosForm, datosFoto:this.datosFoto, timestamp:this.fecha, medida:this.medida, peso:this.peso});
    }

    else{
      this.toast.create({
        message: "Debe haber seleccionado todos los campos para continuar",
        duration: 5000,
        position: 'top'
      }).present();
    }

  }

  atras(){
    this.navCtrl.setRoot(SeleccionPage, {data:this.data, datosForm:this.datosForm, datosFoto:this.datosFoto, timestamp:this.fecha, medida:this.medida, peso:this.peso});
  }

  salir(){
    this.navCtrl.setRoot(HomePage);
  }


}
